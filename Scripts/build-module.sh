#!/bin/bash

cd module
make
cd ../

if test -f "image.ext4"; then
	sudo mount image.ext4 rootfs/
	sudo cp module/hello.ko rootfs/home/
fi
