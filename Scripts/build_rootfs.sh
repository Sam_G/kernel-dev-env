#!/bin/bash

sudo dd if=/dev/zero of=image.ext4 bs=1024 count=624288 status=progress
sudo mkfs.ext4 image.ext4
sudo tune2fs -c0 -i0 image.ext4

mkdir -p rootfs
sudo mount image.ext4 rootfs

sudo /sbin/debootstrap stable rootfs/ http://deb.debian.org/debian/
sudo cp Scripts/change_passwd.sh rootfs/
sudo chroot rootfs/ /bin/bash change_passwd.sh

sudo umount rootfs/
sudo chown sam:sam image.ext4
