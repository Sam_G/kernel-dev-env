#!/bin/bash

cd linux-next/
qemu-system-x86_64 -s -S -kernel arch/x86/boot/bzImage -hda /dev/zero -append "root=/dev/zero console=ttyS0 nokaslr" -serial stdio -display none
