#!/bin/bash

cd linux-next
# qemu-system-x86_64 -kernel arch/x86/boot/bzImage -hda /dev/zero -append "root=/dev/zero console=ttyS0" -serial stdio -display none
qemu-system-x86_64 -kernel arch/x86/boot/bzImage \
	-boot c -m 2049M -hda ../image.ext4 \
	-append "root=/dev/sda rw console=ttyS0,115200 acpi=off nokaslr" \
	-serial stdio -display none
