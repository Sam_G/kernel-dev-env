# Kernel Development Environment

This git repository contains scripts used to setup a development environment for linux kernel and kernel module development. 

# Usage

1) Clone the relevant linux kernel source code. For development on the next kernel, use:

`git clone https://kernel.googlesource.com/pub/scm/linux/kernel/git/next/linux-next.git`

2) Build the kernel using:

`make -j8`

3) Generate a debian-based rootfs image for used with qemu emulation by running:

`bash Scripts/build_rootfs.sh`

4) Run the QEMU virtual machine using:

`bash Scripts/qemu-test.sh`
